{
  description = "flake.nix for physically based simulations";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in
    {
      formatter.${system} = pkgs.nixpkgs-fmt;

      devShell.${system} = pkgs.mkShell rec {
        nativeBuildInputs = with pkgs; [
        roslyn
        cmake
        clang-tools
        glm

        mesa
        openblas

        stdenv.cc  # Use unstable for this package
        libGLU
        xorg.libX11
        xorg.libXrandr
        xorg.libXi
        xorg.libXmu
        xorg.libXinerama
        xorg.libXcursor

        ];

        buildInputs = with pkgs; [ 
        eigen
        ];

        CPATH = pkgs.lib.makeSearchPathOutput "dev" "include" buildInputs;
        LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;
      };
    };
}
