//
// Created by Valentin Vogt on 23.11.23.
//

#ifndef PBS_MYMESH_H
#define PBS_MYMESH_H

#include <utility>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include "MyBaseObj.h"

class MyMesh : public MyBaseObj {
public:
    MyMesh() = default;
    MyMesh(int side_length, Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color);
    ~MyMesh() override = default;

    int getSideLength() const;

private:
    int _side_length;
};

#endif //PBS_MYMESH_H
