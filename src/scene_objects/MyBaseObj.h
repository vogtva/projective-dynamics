//
// Created by Valentin Vogt on 13.12.23.
//

#ifndef PD_MYBASEOBJ_H
#define PD_MYBASEOBJ_H

#include <Eigen/Core>

class MyBaseObj {
public:
    MyBaseObj() = default;
    MyBaseObj(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color);
    virtual ~MyBaseObj() = default;

    int getNumVertices() const;
    int getNumFaces() const;
    int getNumEdges() const;
    double getNodalMass() const;
    Eigen::Vector3d getColor() const;

    void assemble(Eigen::MatrixXd& renderV, Eigen::MatrixXi& renderF, int lg_offset_V,
                          int lg_offset_F);

protected:
    static int edge_count(const Eigen::MatrixXi& F);
    int _num_vertices;
    int _num_faces;
    int _num_internal_faces;
    int _num_edges;

    double _scale;
    double _nodal_mass;
    Eigen::Vector3d _global_position;

    Eigen::MatrixXd _localV;
    Eigen::MatrixXi _localF;

    Eigen::Vector3d _color;
};


#endif //PD_MYBASEOBJ_H
