//
// Created by Valentin Vogt on 23.11.23.
//

#include "MyMesh.h"

#include <utility>


MyMesh::MyMesh(int side_length, Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color)
        : MyBaseObj(std::move(global_position), scale, nodal_mass, std::move(color)) {
    _side_length = side_length;
    _num_vertices = side_length * side_length;
    _num_faces = (side_length - 1) * (side_length - 1) * 2;
    _num_edges = (side_length - 1) * (side_length - 1) + (side_length - 1) * side_length * 2;
    _localV = Eigen::MatrixXd::Zero(_num_vertices, 3);

    int idx = 0;
    for (int i = 0; i < _side_length; ++i) {
        for (int j = 0; j < _side_length; ++j) {
            _localV.row(idx++) = Eigen::Vector3d(i, 0, j);
        }
    }

    _localF = Eigen::MatrixXi::Zero(_num_faces, 3);
    idx = 0;
    for (int i = 0; i < _side_length - 1; ++i) {
        for (int j = 0; j < _side_length - 1; ++j) {
            // redo t1 and t2 in another order to get the correct orientation
            Eigen::Vector3i t1 = Eigen::Vector3i(i * _side_length + j, i * _side_length + j + 1,
                                                 (i + 1) * _side_length + j);
            Eigen::Vector3i t2 = Eigen::Vector3i((i + 1) * _side_length + j, i * _side_length + j + 1,
                                                 (i + 1) * _side_length + j + 1);
            _localF.row(idx++) = t1;
            _localF.row(idx++) = t2;
        }
    }
}

int MyMesh::getSideLength() const {
    return _side_length;
}