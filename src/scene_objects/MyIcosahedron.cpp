//
// Created by Tristan Gabl on 08.12.23.
//

#include "MyIcosahedron.h"

#include <utility>

MyIcosahedron::MyIcosahedron(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color)
               : MyBaseObj(std::move(global_position), scale, nodal_mass, std::move(color)) {
    _num_vertices = 12;

    _localV = Eigen::MatrixXd(_num_vertices, 3);

    // Define the 12 vertices of a unit icosahedron
    double X = 0.525731;
    double Z = 0.850650;
    double N = 0.0;

    _localV << -X, N, Z,
            X,N,Z,
            -X,N,-Z,
            X,N,-Z,
            N,Z,X,
            N,Z,-X,
            N,-Z,X,
            N,-Z,-X,
            Z,X,N,
            -Z,X,N,
            Z,-X,N,
            -Z,-X, N;

    _num_internal_faces = 6;
    _num_faces = 20 + _num_internal_faces;

    _localF = Eigen::MatrixXi(_num_faces, 3);

    // Define the 20 triangles forming the icosahedron and some additional triangles which have an edge to be opposite
    _localF << 0,1,4,
            0,4,9,
            9,4,5,
            4,8,5,
            4,1,8,
            8,1,10,
            8,10,3,
            5,8,3,
            5,3,2,
            2,3,7,
            7,3,10,
            7,10,6,
            7,6,11,
            11,6,0,
            0,6,1,
            6,10,1,
            9,11,0,
            9,2,11,
            9,5,2,
            7,11,2,
            1,2,5,  // internal triangles
            8,11,5,
            10,9,6,
            5,6,9,
            0,3,1,
            4,7,8;

    _num_edges = edge_count(_localF);
}