//
// Created by Tristan Gabl on 24.11.23.
//

#include "MyCube.h"

#include <utility>
#include "igl/edges.h"

MyCube::MyCube(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color)
        : MyBaseObj(std::move(global_position), scale, nodal_mass, std::move(color)) {
    _num_vertices = 8;
    _localV = Eigen::MatrixXd(_num_vertices, 3);

    // Define the 8 vertices of a unit cube
    _localV << 0, 0, 0,
            1, 0, 0,
            1, 1, 0,
            0, 1, 0,
            0, 0, 1,
            1, 0, 1,
            1, 1, 1,
            0, 1, 1;

    _num_internal_faces = 4;
    _num_faces = 12 + _num_internal_faces;
    _localF = Eigen::MatrixXi(_num_faces, 3);

    // Define the 12 triangles forming the cube
    _localF << 0, 1, 2,
            0, 2, 3,
            4, 5, 6,
            4, 6, 7,
            0, 1, 5,
            0, 5, 4,
            2, 3, 7,
            2, 7, 6,
            0, 3, 7,
            0, 7, 4,
            1, 2, 6,
            1, 6, 5,
            1, 3, 7, // internal faces
            1, 7, 5,
            0, 2, 6,
            0, 6, 4;

    _num_edges = edge_count(_localF);
}