//
// Created by Tristan Gabl on 24.11.23.
//
#ifndef PBS_MYCUBE_H
#define PBS_MYCUBE_H

#include "MyBaseObj.h"

#include <Eigen/Core>
#include <Eigen/Sparse>

class MyCube : public MyBaseObj {
public:
    MyCube() = default;
    MyCube(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color);
    ~MyCube() override = default;
};


#endif //PBS_MYCUBE_H
