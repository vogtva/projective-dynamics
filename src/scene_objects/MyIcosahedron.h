//
// Created by Tristan Gabl on 08.12.23.
//

#ifndef PBS_MYICOSAHEDRON_H
#define PBS_MYICOSAHEDRON_H

#include <Eigen/Core>
#include <Eigen/Sparse>
#include "MyBaseObj.h"

class MyIcosahedron : public MyBaseObj {
public:
    MyIcosahedron() = default;
    MyIcosahedron(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color);
    ~MyIcosahedron() override = default;
};


#endif //PBS_MYICOSAHEDRON_H
