//
// Created by Valentin Vogt on 13.12.23.
//

#include "MyBaseObj.h"
#include <igl/edges.h>

MyBaseObj::MyBaseObj(Eigen::Vector3d global_position, double scale, double nodal_mass, Eigen::Vector3d color) {
    _scale = scale;
    _global_position = std::move(global_position);
    _nodal_mass = nodal_mass;
    _color = std::move(color);
}

int MyBaseObj::getNumVertices() const {
    return _num_vertices;
}

int MyBaseObj::getNumFaces() const {
    return _num_faces;
}

int MyBaseObj::getNumEdges() const {
    return _num_edges;
}

double MyBaseObj::getNodalMass() const {
    return _nodal_mass;
}

Eigen::Vector3d MyBaseObj::getColor() const {
    return _color;
}

int MyBaseObj::edge_count(const Eigen::MatrixXi& F) {
    Eigen::MatrixXd E;
    igl::edges(F, E);
    return (int) E.rows();
}

void MyBaseObj::assemble(Eigen::MatrixXd& renderV, Eigen::MatrixXi& renderF, int lg_offset_V,
                         int lg_offset_F) {
    // add global position to rows of V
    Eigen::MatrixXd V = _scale * _localV;
    V = V.rowwise() + _global_position.transpose();

    Eigen::MatrixXi F = _localF;

    renderV.block(lg_offset_V, 0, _num_vertices, 3) = V;
    F = F.array() + lg_offset_V;
    renderF.block(lg_offset_F, 0, _num_faces, 3) = F;
}
