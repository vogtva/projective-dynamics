#include "MeshSim.h"
#include <Eigen/Core>

using triplet = Eigen::Triplet<double>;

void MeshSim::set_constraints() {
    /////////////////////////////////////////////////
    //           Edge length constraints           //
    /////////////////////////////////////////////////

    double edge_length_weight = m_params.edge_weight;
    double position_weight = m_params.position_weight;
    double collision_weight = m_params.collision_weight;

    Eigen::MatrixXi E = m_renderE;
    Eigen::VectorXd weights = Eigen::VectorXd::Constant(E.rows(), edge_length_weight);
    int cube_edges = m_cube.getNumEdges();
    weights.segment(m_mesh.getNumEdges(), cube_edges) *= m_params.sim_objs[0].edge_weight;
    m_edge_length_constraint = edge_length_constraint(E, m_renderV, weights, m_params.edge_strain_tolerance);

    /////////////////////////////////////////////////
    //    Position constraints (fixed vertices)    //
    /////////////////////////////////////////////////

    std::vector<int> constrained_vertices;
    int mesh_side_length = m_mesh.getSideLength();
    int mesh_num_vertices = m_mesh.getNumVertices();
    int mode = m_params.pos_constraint_mode;
    switch (mode) {
        case 0:
            // ONE SIDE
            for (int i = 0; i < mesh_side_length; i++) {
                constrained_vertices.push_back(i);
            }
            break;
        case 1:
            // TWO SIDES
            for (int i = 0; i < mesh_side_length; i++) {
                constrained_vertices.push_back(i);
            }
            for (int i = mesh_num_vertices - mesh_side_length; i < mesh_num_vertices; i++) {
                constrained_vertices.push_back(i);
            }
            break;
        case 2:
            // FOUR SIDES

            for (int i = 0; i < mesh_num_vertices; i++) {
                if (i < mesh_side_length || i >= mesh_num_vertices - mesh_side_length || i % mesh_side_length == 0 ||
                    i % mesh_side_length == mesh_side_length - 1) {
                    constrained_vertices.push_back(i);
                }
            }
            break;
        case 3:
            // MAKE ALL FIXED
            for (int i = 0; i < mesh_num_vertices; i++) {
                constrained_vertices.push_back(i);
            }
            break;
        default:
            break;
    }
    bool taut = m_params.make_taut;
    if (taut) {
        Eigen::VectorXd q_taut = m_q;
        bool version1 = true;
        if (version1) {
            const double taut_constant = m_params.taut_constant;
            q_taut *= taut_constant;
            m_q *= taut_constant;
        } else {
            for (int i = 0; i < mesh_num_vertices; i++) {
                if (i < mesh_side_length) {
                    q_taut.segment<3>(3 * i) += Eigen::Vector3d(-1.0, 0.0, 0.0);
                } else if (i >= mesh_num_vertices - mesh_side_length) {
                    q_taut.segment<3>(3 * i) += Eigen::Vector3d(1.0, 0.0, 0.0);
                } else if (i % mesh_side_length == 0) {
                    q_taut.segment<3>(3 * i) += Eigen::Vector3d(0.0, 0.0, 1.0);
                } else if (i % mesh_side_length == mesh_side_length - 1) {
                    q_taut.segment<3>(3 * i) += Eigen::Vector3d(0.0, 0.0, -1.0);
                }
            }
        }
        m_position_constraint = position_constraint(constrained_vertices, q_taut, position_weight);
    } else {
        m_position_constraint = position_constraint(constrained_vertices, m_q, position_weight);
    }

    /////////////////////////////////////////////////
    //           Collision constraints            //
    /////////////////////////////////////////////////

    std::vector<int> collision_vertices;
    for (int i = mesh_num_vertices; i < m_num_vertices; i++) {
        collision_vertices.push_back(i);
    }
    m_collide_constraint = collide_constraint(collision_vertices, m_renderF, collision_weight);
}

void MeshSim::precompute() {

    // precompute LHS
    SparseMatrix M(m_num_vertices * 3, m_num_vertices * 3);
    M.reserve(Eigen::VectorXi::Constant(m_num_vertices * 3, 1));
    for (int i = 0; i < m_num_vertices * 3; i++) {
        M.insert(i, i) = m_mass(i);
    }
    m_LHS = M / (m_dt * m_dt);

    std::vector<triplet> triplets(12 * m_num_edges);

    if (m_constrain_edges) {
        std::vector<triplet> edge_triplets = m_edge_length_constraint.get_w_SiT_Si();
        triplets.insert(triplets.end(), edge_triplets.begin(), edge_triplets.end());
    }

    if (m_constrain_positions) {
        std::vector<Eigen::Triplet<double>> pos_triplets = m_position_constraint.get_w_SiT_Si();
        triplets.insert(triplets.end(), pos_triplets.begin(), pos_triplets.end());
    }

    SparseMatrix SiT_Si(m_num_vertices * 3, m_num_vertices * 3);
    SiT_Si.setFromTriplets(triplets.begin(), triplets.end());
    m_LHS += SiT_Si;

    m_solver.compute(m_LHS);
}

// solves the perturbed linear system efficiently using the Woodbury matrix identity
// Note that S_collisions is a high thin Matrix ((3 * num_vertices) * (3 * k)) with a small k
// c is a (kxk) matrix with the weights in the middle diagonal
template<typename Solver>
Eigen::VectorXd
advance_with_collision_constraints(Solver& m_solver, const SparseMatrix& S_collisions, Eigen::VectorXd rhs) {
    // solve V * A^-1 * rhs in Woodbury Matrix identity, vector of size 3 * k
    Eigen::VectorXd temp_a = S_collisions.transpose() * m_solver.solve(rhs);
    Eigen::MatrixXd temp_b = S_collisions.transpose() * m_solver.solve(S_collisions);
    temp_b += Eigen::MatrixXd::Identity(static_cast<int>(temp_b.rows()), static_cast<int>(temp_b.cols()));
    Eigen::MatrixXd temp_c = S_collisions * temp_b.fullPivLu().solve(temp_a);
    return m_solver.solve(rhs - temp_c);
}

bool MeshSim::advance() {

    Eigen::VectorXd hot_start = m_q + m_dt * m_vel + m_dt * m_dt * m_force.cwiseProduct(m_inv_mass);

    Eigen::VectorXd q_new = hot_start;

    for (int i = 0; i < m_num_iter; i++) {
        // Local step
        Eigen::VectorXd rhs = m_mass.cwiseProduct(hot_start) / (m_dt * m_dt);

        if (m_constrain_edges) {
            m_edge_length_constraint.add_wSiT_p(q_new, rhs);
        }

        if (m_constrain_positions) {
            m_position_constraint.add_wSiT_p(q_new, rhs);
        }
        // Global step
        auto [n_collisions, S] = m_collide_constraint.handle_collisions(m_q, q_new, rhs);
        if (n_collisions) {
            // std::cout << n_collisions << " collisions detected" << std::endl;
            // empty dummy variables, the simulation should run with them
            // this works and is about 3 time slower, which makes sense because it uses m_solver 3 times 
            SparseMatrix S_mat(m_num_vertices * 3, 3 * n_collisions);
            S_mat.setFromTriplets(S.begin(), S.end());
            q_new = advance_with_collision_constraints(m_solver, S_mat, rhs);

        } else {
            q_new = m_solver.solve(rhs);
        }
    }

    m_vel = (q_new - m_q) / m_dt;

    if (filter_vel) {
        double max_vel = 1e2;
        for (int i = 0; i < m_num_vertices; i++) {
            double vel_norm = m_vel.segment<3>(3 * i).norm();
            if (vel_norm > max_vel) {
                m_vel.segment<3>(3 * i) /= vel_norm / max_vel;
            }
        }
    }
    m_q = q_new;

    // advance m_time
    m_time += m_dt;
    m_step++;

    return false;
}

void MeshSim::renderRenderGeometry(igl::opengl::glfw::Viewer& viewer) {
    viewer.data().set_mesh(m_renderV, m_renderF);

    Eigen::MatrixXd face_colors = Eigen::MatrixXd::Zero(m_renderF.rows(), 3);

    for (int i = 0; i < m_mesh.getNumFaces(); ++i) {
        face_colors.row(i) = m_mesh.getColor();
    }

    int offset = m_mesh.getNumFaces();
    for (auto& obj: m_objs) {
        int num_faces = obj->getNumFaces();

        for (int i = 0; i < num_faces; i++) {
            face_colors.row(i + offset) = obj->getColor();
        }

        offset += num_faces;
    }

    viewer.data().set_colors(face_colors);
}

void MeshSim::render_positions(Eigen::VectorXd& q) {
    Eigen::MatrixXd tmp = Eigen::Map<Eigen::MatrixXd>(q.data(), 3, q.size() / 3).transpose();
    m_renderV = tmp;
}

