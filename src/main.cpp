#include <igl/writeOFF.h>
#include <thread>
#include "Gui.h"
#include "MeshSim.h"

class MeshGui : public Gui {
   public:
    // simulation parameters
    float m_dt;

    MeshSim *p_sim = NULL;

    explicit MeshGui(params p) {
        // initialize the spring to be used
        m_dt = p.timestep;
        p_sim = new MeshSim(p);
        setSimulation(p_sim);

        start();
    }

    void updateSimulationParameters() override {
        // change all parameters of the simulation to the values that are set in
        // the GUI
        p_sim->setTimestep(m_dt);
    }

    void clearSimulation() override {}

    void drawSimulationParameterMenu() override {
    }
};

int main(int argc, char *argv[]) {

//    sim_obj cube1 = {
//        .type = 0,
//        .pos = Eigen::Vector3d(10., 5., 10.),
//        .scale = 0.2,
//        .edge_weight = 1e6,
//        .nodal_mass = 5.0,
//        .color = Eigen::Vector3d(10.0, 5.0, 10.0)
//    };
    std::vector<sim_obj> sim_objs;
    for (int i = 0; i < 10; ++i) {
        sim_obj ob = {
                .type = 1,
                .pos = Eigen::Vector3d(5. + i, 5., 5 + i),
                .scale = 0.5,
                .edge_weight = 1.0,
                .nodal_mass = 0.075,
                .color = Eigen::Vector3d(0.0, 0.0, 1.0)
        };
        sim_objs.push_back(ob);
    }

//    sim_obj ico1 = {
//        .type = 1,
//        .pos = Eigen::Vector3d(12., 15., 16.),
//        .scale = 1.0,
//        .edge_weight = 1e6,
//        .nodal_mass = 10.0,
//        .color = Eigen::Vector3d(1.0, 0.0, 0.0)
//    };
//    sim_obj ico2 = {
//            .type = 1,
//            .pos = Eigen::Vector3d(10., 10., 11.),
//            .scale = 1.0,
//            .edge_weight = 1e6,
//            .nodal_mass = 10.0,
//            .color = Eigen::Vector3d(1.0, 0.0, 0.0)
//    };
//    sim_obj ico3 = {
//            .type = 1,
//            .pos = Eigen::Vector3d(10., 8., 15.),
//            .scale = 1.0,
//            .edge_weight = 1e6,
//            .nodal_mass = 10.0,
//            .color = Eigen::Vector3d(1.0, 0.0, 0.0)
//    };

    params p = {
        .mesh_side_length = 30,
        .mesh_scale = 20./30,

        .position_weight = 1e9,
        .edge_weight = 1e4,
        .edge_strain_tolerance = 0.0,
        .collision_weight = 1e9,

        .timestep = 0.007,
        .num_iterations = 1,

        .sim_objs = sim_objs,

        .pos_constraint_mode = 2,

        .make_taut = true,
        .taut_constant = 1.1
    };
    new MeshGui(p);

    return 0;
}
