//
// Created by Valentin Vogt on 23.11.23.
//

#include "edge_length_constraint.h"

using triplet = Eigen::Triplet<double>;

edge_length_constraint::edge_length_constraint(Eigen::MatrixXi &edges, Eigen::MatrixXd &vertices, Eigen::VectorXd weights, double strain_limit) :
        m_edges(edges), m_weights(weights), m_strain_limit(strain_limit) {
    m_num_constraints = edges.rows();
    m_rest_lengths.resize(m_num_constraints);
    for (int i = 0; i < m_num_constraints; ++i) {
        Eigen::Vector3d v1 = vertices.row(edges(i, 0));
        Eigen::Vector3d v2 = vertices.row(edges(i, 1));
        m_rest_lengths(i) = (v1 - v2).norm();
    }
}

std::vector<triplet> edge_length_constraint::get_w_SiT_Si() const {
    std::vector<triplet> triplets(m_num_constraints * 12);

//#pragma omp parallel for default(none) shared(triplets) firstprivate(m_num_constraints, m_edges, m_weights)
    for (int i = 0; i < m_num_constraints; ++i) {
        triplets.at(12 * i + 0) = triplet(3 * m_edges(i, 0) + 0, 3 * m_edges(i, 0) + 0, m_weights(i));
        triplets.at(12 * i + 1) = triplet(3 * m_edges(i, 0) + 1, 3 * m_edges(i, 0) + 1, m_weights(i));
        triplets.at(12 * i + 2) = triplet(3 * m_edges(i, 0) + 2, 3 * m_edges(i, 0) + 2, m_weights(i));
        triplets.at(12 * i + 3) = triplet(3 * m_edges(i, 1) + 0, 3 * m_edges(i, 1) + 0, m_weights(i));
        triplets.at(12 * i + 4) = triplet(3 * m_edges(i, 1) + 1, 3 * m_edges(i, 1) + 1, m_weights(i));
        triplets.at(12 * i + 5) = triplet(3 * m_edges(i, 1) + 2, 3 * m_edges(i, 1) + 2, m_weights(i));
        triplets.at(12 * i + 6) = triplet(3 * m_edges(i, 0) + 0, 3 * m_edges(i, 1) + 0, -m_weights(i));
        triplets.at(12 * i + 7) = triplet(3 * m_edges(i, 0) + 1, 3 * m_edges(i, 1) + 1, -m_weights(i));
        triplets.at(12 * i + 8) = triplet(3 * m_edges(i, 0) + 2, 3 * m_edges(i, 1) + 2, -m_weights(i));
        triplets.at(12 * i + 9) = triplet(3 * m_edges(i, 1) + 0, 3 * m_edges(i, 0) + 0, -m_weights(i));
        triplets.at(12 * i + 10) = triplet(3 * m_edges(i, 1) + 1, 3 * m_edges(i, 0) + 1, -m_weights(i));
        triplets.at(12 * i + 11) = triplet(3 * m_edges(i, 1) + 2, 3 * m_edges(i, 0) + 2, -m_weights(i));
    }
    return triplets;
}

void edge_length_constraint::add_wSiT_p(Eigen::VectorXd &q, Eigen::VectorXd &rhs) const {

//#pragma omp parallel for default(none) shared(rhs) firstprivate(q, m_num_constraints, m_edges, m_rest_lengths, m_weights, m_strain_limit)
    for (int i = 0; i < m_num_constraints; ++i) {
        Eigen::Vector3d v1 = q.segment<3>(3 * m_edges(i, 0));
        Eigen::Vector3d v2 = q.segment<3>(3 * m_edges(i, 1));
        Eigen::Vector3d edge = v1 - v2;
        double edge_length = edge.norm();

        Eigen::Vector3d p;
        if (edge_length < m_rest_lengths(i) * (1.0 + m_strain_limit) && edge_length > m_rest_lengths(i) * (1.0 - m_strain_limit)) {
            p = edge;
        } else {
            p = edge.normalized() * m_rest_lengths(i);
        }

        rhs.segment<3>(3 * m_edges(i, 0)) += m_weights(i) * p;
        rhs.segment<3>(3 * m_edges(i, 1)) -= m_weights(i) * p;
    }
}
