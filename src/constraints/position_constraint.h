//
// Created by Valentin Vogt on 23.11.23.
//

#ifndef PBS_POSITION_CONSTRAINT_H
#define PBS_POSITION_CONSTRAINT_H

#include <Eigen/Core>
#include <Eigen/Sparse>
using SparseMatrix = Eigen::SparseMatrix<double>;

class position_constraint {
public:
    position_constraint() = default;
    explicit position_constraint(const std::vector<int>& vertex_indices, Eigen::VectorXd rest_q, double w = 1.0);

    void add_wSiT_p(Eigen::VectorXd &q, Eigen::VectorXd &rhs) const;
    std::vector<Eigen::Triplet<double>> get_w_SiT_Si() const;

private:
    int m_num_constraints;
    std::vector<int> m_indices;
    double m_weight;
    Eigen::MatrixXd m_rest_position;
};


#endif //PBS_POSITION_CONSTRAINT_H
