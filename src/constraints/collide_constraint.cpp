//
// Created by Valentin Vogt on 24.11.23.
//

#include "collide_constraint.h"
#include <Eigen/Dense>
#include <iostream>
#include <Eigen/Sparse>

using triplet = Eigen::Triplet<double>;

bool collide_constraint::step_intersects_triangle(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2,
                                                  Eigen::Matrix3d& triangle, Eigen::Vector3d& intersection) {
    bool print = false;
//    if (pos1(1) > 0.0 && pos2(1) < 0.0) {
//        print = true;
//    }
    double EPSILON = 0.0000001;
    Eigen::Vector3d edge1 = triangle.col(1) - triangle.col(0);
    Eigen::Vector3d edge2 = triangle.col(2) - triangle.col(0);

    Eigen::Vector3d dir = pos2 - pos1;
    Eigen::Vector3d dir_norm = dir.normalized();

    Eigen::Vector3d h = dir_norm.cross(edge2);
    double det = edge1.dot(h);

    if (print) {
        std::cout << "edge1: " << edge1.transpose() << std::endl;
        std::cout << "edge2: " << edge2.transpose() << std::endl;
        std::cout << "det: " << det << std::endl;
    }
    if (det > -EPSILON && det < EPSILON) {
        return false;
    }
    Eigen::Vector3d s = pos1 - triangle.col(0);
    double inv_det = 1.0 / det;
    double u = s.dot(h) * inv_det;
    if (print) {
        std::cout << "s: " << s.transpose() << std::endl;
        std::cout << "inv_det: " << inv_det << std::endl;
        std::cout << "u: " << u << std::endl;
    }
    if (u < 0.0 || u > 1.0) {
        return false;
    }
    Eigen::Vector3d q = s.cross(edge1);
    double v = dir_norm.dot(q) * inv_det;
    if (print) {
        std::cout << "q: " << q.transpose() << std::endl;
        std::cout << "v: " << v << std::endl;
    }
    if (v < 0.0 || u + v > 1.0) {
        return false;
    }
    double t = edge2.dot(q) * inv_det;
    if (print) {
        std::cout << "t: " << t << std::endl;
    }
    if (t > EPSILON && t < sqrt(dir.dot(dir))) {
        intersection = pos1 + t * dir_norm;
        return true;
    }
    return false;
}

std::tuple<int, std::vector<triplet>>
collide_constraint::handle_collisions(Eigen::VectorXd q_old, Eigen::VectorXd& q_new, Eigen::VectorXd& rhs) {
    std::vector<triplet> S;
    int n_constraints = 0;
    // #pragma omp parallel for collapse(2) default(none) shared(q_new) firstprivate(q_old, m_num_constraints, m_num_triangles, m_vertex_indices, m_triangles)
    for (int i = 0; i < m_num_constraints; i++) {
        for (int t = 0; t < m_num_triangles; t++) {
            Eigen::Vector3d pos1 = q_old.segment<3>(m_vertex_indices[i] * 3);
            Eigen::Vector3d pos2 = q_new.segment<3>(m_vertex_indices[i] * 3);
            Eigen::Matrix3d triangle;
            triangle.col(0) = q_old.segment<3>(m_triangles(t, 0) * 3);
            triangle.col(1) = q_old.segment<3>(m_triangles(t, 1) * 3);
            triangle.col(2) = q_old.segment<3>(m_triangles(t, 2) * 3);


            Eigen::Vector3d intersection;
            if (step_intersects_triangle(pos1, pos2, triangle, intersection)) {
                // project the new position above the triangle with a fixed difference
                // if you project it right on the triangle, the cube slips through the triangle at some point
                // for that, i'm first computing the norm of the triangle and then reflect the point on the triangle
                Eigen::Vector3d tria_norm = ((triangle.col(1) - triangle.col(0)).cross(triangle.col(2) - triangle.col(0))).normalized();


                Eigen::Vector3d new_position = pos2;
                Eigen::Vector3d shift = pos2 - intersection;
                double min_shift = 0.08;
                // preventing objects from falling through trampolin
                if (shift.norm() < min_shift) {
                    shift = shift.normalized() * min_shift;
                }
                // if the object is below the trampoline, it gets projected above it
                if (shift.dot(-tria_norm) > 0 ) {
                    new_position += shift.dot(-tria_norm) * tria_norm * 2;
                }

                // double shift_triangle = (q_new(m_triangles(t, 0) * 3 + 1) - q_old(m_triangles(t, 0) * 3 + 1)
                //                          + q_new(m_triangles(t, 1) * 3 + 1) - q_old(m_triangles(t, 1) * 3 + 1)
                //                          + q_new(m_triangles(t, 2) * 3 + 1) - q_old(m_triangles(t, 2) * 3 + 1)) / 3.;


                // // in case that the triangle gets higher
                // if (shift_triangle > 0) {
                //     new_position(1) += shift_triangle;
                // }

                // triangle raises faster than object is falling
                if (shift(1) > 0) {
                    shift << 0, 0, 0;
                }

                rhs.segment<3>(m_triangles(t, 0) * 3) += (q_old.segment<3>(m_triangles(t, 0) * 3) + shift) * m_weight;
                rhs.segment<3>(m_triangles(t, 1) * 3) += (q_old.segment<3>(m_triangles(t, 1) * 3) + shift) * m_weight;
                rhs.segment<3>(m_triangles(t, 2) * 3) += (q_old.segment<3>(m_triangles(t, 2) * 3) + shift) * m_weight;

                rhs.segment<3>(m_vertex_indices[i] * 3) += new_position * m_weight;

                S.emplace_back(3 * m_vertex_indices[i] + 0, 0 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_vertex_indices[i] + 1, 1 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_vertex_indices[i] + 2, 2 + 3 * n_constraints++, std::sqrt(m_weight));

                S.emplace_back(3 * m_triangles(t, 0) + 0, 0 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 0) + 1, 1 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 0) + 2, 2 + 3 * n_constraints++, std::sqrt(m_weight));

                S.emplace_back(3 * m_triangles(t, 1) + 0, 0 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 1) + 1, 1 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 1) + 2, 2 + 3 * n_constraints++, std::sqrt(m_weight));

                S.emplace_back(3 * m_triangles(t, 2) + 0, 0 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 2) + 1, 1 + 3 * n_constraints, std::sqrt(m_weight));
                S.emplace_back(3 * m_triangles(t, 2) + 2, 2 + 3 * n_constraints++, std::sqrt(m_weight));
            }
        }
    }

    return std::tuple<int, std::vector<triplet>>{n_constraints, S};
}
