//
// Created by Valentin Vogt on 24.11.23.
//

#ifndef PBS_COLLIDE_CONSTRAINT_H
#define PBS_COLLIDE_CONSTRAINT_H

#include <Eigen/Core>
#include <Eigen/Sparse>

using SparseMatrix = Eigen::SparseMatrix<double>;

class collide_constraint {
public:
    collide_constraint() = default;

    collide_constraint(const std::vector<int>& vertex_indices, const Eigen::MatrixXi& triangles, double weight) :
            m_vertex_indices(vertex_indices), m_triangles(triangles), m_weight(weight) {
        m_num_constraints = m_vertex_indices.size();
        m_num_triangles = m_triangles.rows();
    }

    static bool step_intersects_triangle(const Eigen::Vector3d& pos1, const Eigen::Vector3d& pos2,
                                         Eigen::Matrix3d& triangle, Eigen::Vector3d& intersection);

    std::tuple<int, std::vector<Eigen::Triplet<double>>>
    handle_collisions(Eigen::VectorXd q_old, Eigen::VectorXd& q_new, Eigen::VectorXd& rhs);
private:
    Eigen::MatrixXi m_triangles;
    std::vector<int> m_vertex_indices;
    int m_num_constraints;
    int m_num_triangles;
    double m_weight;
};

#endif //PBS_COLLIDE_CONSTRAINT_H
