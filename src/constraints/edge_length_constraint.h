//
// Created by Valentin Vogt on 23.11.23.
//

#ifndef PBS_EDGE_LENGTH_CONSTRAINT_H
#define PBS_EDGE_LENGTH_CONSTRAINT_H

#include <Eigen/Core>
#include <Eigen/Sparse>

using SparseMatrix = Eigen::SparseMatrix<double>;

class edge_length_constraint {
public:
    edge_length_constraint() = default;
    edge_length_constraint(Eigen::MatrixXi &edges, Eigen::MatrixXd &vertices, Eigen::VectorXd weights, double strain_limit = 0.01);

    void add_wSiT_p(Eigen::VectorXd &q, Eigen::VectorXd &rhs) const;

    std::vector<Eigen::Triplet<double>> get_w_SiT_Si() const;

private:
    int m_num_constraints;
    Eigen::MatrixXi m_edges;
    Eigen::VectorXd m_weights;
    Eigen::VectorXd m_rest_lengths;
    double m_strain_limit;
};


#endif //PBS_EDGE_LENGTH_CONSTRAINT_H
