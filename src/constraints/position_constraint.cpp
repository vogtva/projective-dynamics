//
// Created by Valentin Vogt on 23.11.23.
//

#include "position_constraint.h"

using triplet = Eigen::Triplet<double>;

position_constraint::position_constraint(const std::vector<int>& vertex_indices, Eigen::VectorXd rest_q, double w) :
        m_indices(vertex_indices), m_weight(w) {
    m_num_constraints = m_indices.size();
    m_rest_position.resize(3, m_num_constraints);
    for (int i = 0; i < m_num_constraints; ++i) {
        m_rest_position.col(i) = rest_q.segment<3>(3 * m_indices[i]);
    }
}

std::vector<triplet> position_constraint::get_w_SiT_Si() const {
    std::vector<triplet> w_SiT_Si(m_num_constraints * 3);

//#pragma omp parallel for default(none) shared(w_SiT_Si) firstprivate(m_indices, m_weight, m_num_constraints)
    for (int i = 0; i < m_num_constraints; ++i) {
        w_SiT_Si.at(3 * i + 0) = triplet(3 * m_indices[i] + 0, 3 * m_indices[i] + 0, m_weight);
        w_SiT_Si.at(3 * i + 1) = triplet(3 * m_indices[i] + 1, 3 * m_indices[i] + 1, m_weight);
        w_SiT_Si.at(3 * i + 2) = triplet(3 * m_indices[i] + 2, 3 * m_indices[i] + 2, m_weight);
    }

    return w_SiT_Si;
}

void position_constraint::add_wSiT_p(Eigen::VectorXd &q, Eigen::VectorXd &rhs) const {

//#pragma omp parallel for default(none) shared(rhs) firstprivate(m_indices, m_weight, m_rest_position, m_num_constraints)
    for (int i = 0; i < m_num_constraints; ++i) {
        rhs.segment<3>(3 * m_indices[i]) += m_weight * m_rest_position.col(i);
    }
}

