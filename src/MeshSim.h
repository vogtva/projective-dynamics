#ifndef MESH_SIM_H
#define MESH_SIM_H

#include "Simulation.h"
#include "scene_objects/MyBaseObj.h"
#include "scene_objects/MyMesh.h"
#include "scene_objects/MyCube.h"
#include "scene_objects/MyIcosahedron.h"

#include "constraints/edge_length_constraint.h"
#include "constraints/position_constraint.h"
#include "constraints/collide_constraint.h"

#include <iostream>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <igl/edges.h>

using SparseMatrix = Eigen::SparseMatrix<double>;

struct sim_obj {
    int type; // 0: cube, 1: icosahedron
    Eigen::Vector3d pos;
    double scale;
    double edge_weight;
    double nodal_mass;
    Eigen::Vector3d color;
};


struct params {
    int mesh_side_length;
    double mesh_scale;

    double position_weight;
    double edge_weight;
    double edge_strain_tolerance;
    double collision_weight;
    double timestep;
    int num_iterations;

    std::vector<sim_obj> sim_objs;

    int pos_constraint_mode; // one_side, two_sides, four_sides, all_fixed

    bool make_taut;
    double taut_constant;
};

class MeshSim : public Simulation {
public:
    explicit MeshSim(const params& p) : Simulation() {
        m_params = p;

        init(p);
        set_constraints();
        precompute();
    }

    void init(const params& p) {
        // mesh and positions
        int n = p.mesh_side_length;
        m_mesh = MyMesh(n, Eigen::Vector3d(0.0, 0.0, 0.0), p.mesh_scale, 1.0, Eigen::Vector3d(0.0, 1.0, 0.0));
        int num_vertices_mesh = m_mesh.getNumVertices();
        m_num_vertices = num_vertices_mesh;

        for (auto& obj : p.sim_objs) {
            if (obj.type == 0) {
                auto cube = new MyCube(obj.pos, obj.scale, obj.nodal_mass, obj.color);
                m_objs.push_back(cube);
                m_num_vertices += cube->getNumVertices();
            } else if (obj.type == 1) {
                auto icosahedron = new MyIcosahedron(obj.pos, obj.scale, obj.nodal_mass, obj.color);
                m_objs.push_back(icosahedron);
                m_num_vertices += icosahedron->getNumVertices();
            } else {
                std::cerr << "Invalid sim_obj type " << obj.type << std::endl;
            }
        }

        // contains coords of all vertices in its rows
        m_renderV = Eigen::MatrixXd(m_num_vertices, 3);

        int num_faces_mesh = m_mesh.getNumFaces();
        m_num_faces = num_faces_mesh;
        for (auto& obj : m_objs) {
            m_num_faces += obj->getNumFaces();
        }

        // contains indices of all faces in its rows
        m_renderF = Eigen::MatrixXi(m_num_faces, 3);

        // use scene object member function to assemble renderV and renderF
        m_mesh.assemble(m_renderV, m_renderF, 0, 0);
        int v_offset = m_mesh.getNumVertices();
        int f_offset = m_mesh.getNumFaces();
        for (auto & m_obj : m_objs) {
            m_obj->assemble(m_renderV, m_renderF, v_offset, f_offset);
            v_offset += m_obj->getNumVertices();
            f_offset += m_obj->getNumFaces();
        }

        igl::edges(m_renderF, m_renderE);
        m_num_edges = m_renderE.rows();

        Eigen::MatrixXd V = m_renderV.transpose();
        m_q = Eigen::Map<Eigen::VectorXd>(V.data(), V.size());

        // initialize physical quantities
        m_initial_q = m_q;
        m_vel = Eigen::VectorXd::Zero(m_num_vertices * 3);
        m_mass = Eigen::VectorXd::Ones(m_num_vertices * 3);
        int offset = m_mesh.getNumVertices();
        for (auto& obj : m_objs) {
            m_mass.segment(3 * offset, 3 * obj->getNumVertices()) *= obj->getNodalMass();
            offset += obj->getNumVertices();
        }
        m_inv_mass = m_mass.cwiseInverse();

        Eigen::VectorXd g3(3);
        g3 << 0.0, -10, 0.0;
        Eigen::VectorXd g_acc = g3.replicate(m_num_vertices, 1);
        m_force = m_mass.cwiseProduct(g_acc);

        m_dt = p.timestep;
        m_num_iter = p.num_iterations;

        reset();
    }

    void set_constraints();

    void precompute();

    void resetMembers() override {
        m_q = m_initial_q;
        m_vel = Eigen::VectorXd::Zero(m_num_vertices * 3);
    }

    void updateRenderGeometry() override {
        // update render geometry
        render_positions(m_q);
    }

    bool advance() override;

    void renderRenderGeometry(igl::opengl::glfw::Viewer& viewer) override;
private:
    void render_positions(Eigen::VectorXd &q);
    int m_step = 0;

    bool m_constrain_edges = true;
    bool m_constrain_positions = true;
    bool filter_vel = true;
    Eigen::VectorXd m_q;
    Eigen::VectorXd m_initial_q;
    Eigen::VectorXd m_vel;
    Eigen::VectorXd m_force;
    Eigen::VectorXd m_mass;
    Eigen::VectorXd m_inv_mass;

    int m_num_iter;

    SparseMatrix m_LHS;
    Eigen::SimplicialLDLT<SparseMatrix> m_solver;

    // scene objects
    MyMesh m_mesh;
    MyCube m_cube;
    MyIcosahedron m_icosahedron;

    std::vector<MyBaseObj *> m_objs;

    int m_num_vertices;
    int m_num_faces;
    int m_num_edges;

    position_constraint m_position_constraint;
    edge_length_constraint m_edge_length_constraint;
    collide_constraint m_collide_constraint;

    params m_params;

    Eigen::MatrixXd m_renderV;
    Eigen::MatrixXi m_renderF;
    Eigen::MatrixXi m_renderE;
};

#endif // MESH_SIM_H