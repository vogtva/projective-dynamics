# Projective Dynamics

## Building

```
mkdir build
cd build
cmake ..
make all

./pd_mesh
``````

## Setting up the simulation

The simulation parameters are set in `src/main.cpp`. The fields of the `params` struct should be mostly self-explanatory. `sim_objs` should be a vector of 
```c++
struct sim_obj {
    int type; // 0: cube, 1: icosahedron
    Eigen::Vector3d pos;
    double scale;
    double edge_weight;
    double nodal_mass;
    Eigen::Vector3d color; // R G B (0 to 1)
};
```
If `params.make_taut` is true, the mesh positions are scaled by `taut_constant` to simulate the mesh being stretched. `pos_constraint_mode` defines where positional constraints are applied to the mesh and can take the following values:
```
0: one edge
1: two (opposite) edges
2: all four edges
3: every vertex
```
